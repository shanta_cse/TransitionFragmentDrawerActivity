package com.example.shantacse.coxtunes_news_app;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class Splash_screen extends AppCompatActivity {


    private static int splash_screen=2500;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        new android.os.Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                Intent i=new Intent(Splash_screen.this,MainActivity.class);
                startActivity(i);

            }
        },splash_screen);
    }
}
